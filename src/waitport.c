/*
 * waitport.c  - Probe TCP port until connection succedees.
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE     200112L
#endif /* _POSIX_C_SOURCE */

#include <errno.h>
#include <locale.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/time.h>
#include <unistd.h>

#include "config.h"
#include "gettext.h"
#include "help.h"
#include "resolver.h"
#include "settings.h"
#include "tcp.h"

#ifndef WAITPORT_READ_BUFFER_LENGTH
#define WAITPORT_READ_BUFFER_LENGTH     1024
#endif  /* WAITPORT_READ_BUFFER_LENGTH */


int  main(int  argc, char * const  argv[])
{
    int                 err;
    unsigned long int   try;
    struct addrinfo     alive;
    char                mark, ip[INET6_ADDRSTRLEN], rbuf[WAITPORT_READ_BUFFER_LENGTH];
    Settings            *settings;
    Status              status;
    Resolver            *resolver;


    setlocale(LC_ALL, "");
#ifdef GETTEXT
    bindtextdomain(PACKAGE_NAME, LOCALEDIR);
    textdomain(PACKAGE_NAME);
#endif /* GETTEXT */

    settings = settings_new();
    err = settings_parse_args(argc, argv, settings);
    if ( err != 0 || IS_SET(settings->flags, SETTINGS_FLAG_HELP) ) {
        help_print(err ? stderr : stdout);
        free(settings);
        return err;
    };

    if ( IS_SET(settings->flags, SETTINGS_FLAG_VERSION) ) {
        help_print_version(stdout);
        free(settings);
        return 0;
    };

    /*
     * Because we can't distinguish between error resolving host name (which is OK here)
     * and error resolving service name (which is an invalid argument passed by the user),
     * we have to check the latter first and fail early.
     */
    if ( !resolver_check_service_name(settings->service) ) {
        fprintf(stderr, "Service name could not be resolved: %s\n", settings->service);
        return 100;
    };

    if ( IS_SET(settings->flags, SETTINGS_FLAG_VERBOSE) ) {
        fprintf(stdout, _("Probing service %s on host %s"), settings->service, settings->hostname);
        if ( settings->numtries > 0 )
            fprintf(stdout, __(" at most %lu time,", " at most %lu times,", settings->numtries), settings->numtries);
        if ( IS_SET(settings->flags, SETTINGS_FLAG_NOCACHE) )
            fprintf(stdout, _(" requerying DNS with each probe,"));
        fprintf(stdout, __(" with %lu second timeout", " with %lu seconds timeout", settings->timeout), settings->timeout);
        fprintf(stdout, __(" and %lu second wait time.\n", " and %lu seconds wait time.\n", settings->wait), settings->wait);
    };

    memset(&alive, 0, sizeof(struct addrinfo));
    resolver = resolver_new(IS_SET(settings->flags, SETTINGS_FLAG_NOCACHE));

    status = TCP_STATUS_UNKNOWN;
    try = 1;

    do {
        if ( (status = resolver_query(resolver, settings->hostname, settings->service)) == TCP_STATUS_SUCCESS )
            status = tcp_try_connect(resolver->results, &alive, settings->timeout, rbuf,
                                     IS_SET(settings->flags, SETTINGS_FLAG_CONNECT_ONLY) ? 0 : sizeof(rbuf));

        if ( !IS_SET(settings->flags, SETTINGS_FLAG_QUIET) ) {
            switch ( status ) {
                case TCP_STATUS_UNKNOWN:      mark = '.'; break;
                case TCP_STATUS_SUCCESS:      mark = '!'; break;
                case TCP_STATUS_SYSTEM_ERROR: mark = 'E'; break;
                case TCP_STATUS_NAME_ERROR:   mark = 'D'; break;
                case TCP_STATUS_READ_ERROR:   mark = 'r'; break;
                case TCP_STATUS_TIMEOUT:      mark = 'T'; break;
                default:                      mark = '?'; break;
            };

            fprintf(stdout, "%c", mark);
            fflush(stdout);
        };

        if ( (settings->numtries > 0 && try == settings->numtries) || status == TCP_STATUS_SUCCESS ) {
            break;
        } else {
            sleep(settings->wait);
            try++;
        };
    } while ( status != TCP_STATUS_SUCCESS );

    resolver_free(resolver);

    if ( status == TCP_STATUS_SUCCESS && !IS_SET(settings->flags, SETTINGS_FLAG_QUIET) ) {
        tcp_addr_to_string(&alive, ip, sizeof(ip));
        fprintf(stdout, __("\nConnection to %s succeeded after %lu try\n",
                           "\nConnection to %s succeeded after %lu tries\n", try), ip, try);
    } else if ( status != TCP_STATUS_GAI_ERROR && !IS_SET(settings->flags, SETTINGS_FLAG_QUIET) ) {
        fprintf(stderr, __("\nConnection unsuccessful after %lu try\n",
                           "\nConnection unsuccessful after %lu tries\n", try), try);
    };

    free(settings);
    return status != TCP_STATUS_SUCCESS;
}

/* vim: set filetype=c expandtab tabstop=4 sts=4 shiftwidth=4: */
