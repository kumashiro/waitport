/*
 * resolver.c  - Domain name and service resolution functions.
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE     200112L
#endif /* _POSIX_C_SOURCE */

#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include "resolver.h"
#include "tcp.h"
#include "util.h"


Resolver  *resolver_new(int  nocache)
{
    Resolver        *resolver;


    if ( (resolver = (Resolver*)malloc(sizeof(Resolver))) == NULL )
        abort();

    memset(&(resolver->filter), 0, sizeof(struct addrinfo));
    resolver->filter.ai_family = AF_UNSPEC;
    resolver->filter.ai_socktype = SOCK_STREAM;
    resolver->filter.ai_flags = AI_PASSIVE;
    resolver->nocache = nocache;
    resolver->results = NULL;

    return resolver;
}


int  resolver_query(Resolver  *resolver, const char  *hostname, const char  *service)
{
    int         gaierr;


    if ( resolver->nocache && resolver->results != NULL )
        resolver_reset(resolver);

    if ( resolver->nocache || resolver->results == NULL ) {
        gaierr = getaddrinfo(hostname, service, &(resolver->filter), &(resolver->results));
        if ( gaierr == 0 )
            return TCP_STATUS_SUCCESS;
        else if ( resolver->results == NULL )
            return TCP_STATUS_NAME_ERROR;

        return gaierr != EAI_SYSTEM ? TCP_STATUS_GAI_ERROR : TCP_STATUS_SYSTEM_ERROR;
    } else {
        return TCP_STATUS_SUCCESS;
    };
}


void  resolver_reset(Resolver  *resolver)
{
    freeaddrinfo(resolver->results);
    resolver->results = NULL;
}


void  resolver_free(Resolver  *resolver)
{
    resolver_reset(resolver);
    free(resolver);
}


inline
int  resolver_check_service_name(const char  *service)
{
    unsigned long   numeric;
    char            *end;


    if ( util_str2ul(service, &numeric, &end) == 0 && numeric < 65535 )
        return 1;

    return getservbyname(service, "tcp") != NULL;
}

/* vim: set ft=c sw=4 sts=4 et: */
