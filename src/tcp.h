/*
 * tcp.h  - TCP connection functions (header file).
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef TCP_H
#define TCP_H


typedef enum {
    TCP_STATUS_UNKNOWN,
    TCP_STATUS_SUCCESS,
    TCP_STATUS_SYSTEM_ERROR,
    TCP_STATUS_GAI_ERROR,
    TCP_STATUS_NAME_ERROR,
    TCP_STATUS_READ_ERROR,
    TCP_STATUS_TIMEOUT
} Status;


void  tcp_addr_to_string(struct addrinfo  *address, char  *buffer, size_t  buflen);
Status  tcp_try_connect(struct addrinfo  *addresses, struct addrinfo  *alive, long  timeout, char  *rbuf, size_t  rbuf_len);

#endif /* TCP_H */
/* vim: set ft=c sw=4 sts=4 et: */
