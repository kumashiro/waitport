/*
 * settings.c  - Argument parsing functions.
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "config.h"
#include "gettext.h"
#include "settings.h"
#include "util.h"


#define ARGSPEC     "chn:qrt:Vvw:"

#ifdef HAVE_GETOPT_LONG
static struct option    long_options[] = {
    {"connect-only",      no_argument, NULL, 'c'},
    {"help",              no_argument, NULL, 'h'},
    {"tries",       required_argument, NULL, 'n'},
    {"quiet",             no_argument, NULL, 'q'},
    {"requery-dns",       no_argument, NULL, 'r'},
    {"timeout",     required_argument, NULL, 't'},
    {"version",           no_argument, NULL, 'V'},
    {"verbose",           no_argument, NULL, 'v'},
    {"wait",        required_argument, NULL, 'w'},
    {NULL,                          0, NULL,   0}
};
#endif /* HAVE_GETOPT_LONG */


int  settings_parse_args(int  argc, char * const  *argv, Settings  *settings)
{
    int         arg;
    char        *endptr;


    do {
#ifdef HAVE_GETOPT_LONG
        arg = getopt_long(argc, argv, ARGSPEC, long_options, NULL);
#else
        arg = getopt(argc, argv, ARGSPEC);
#endif /* HAVE_GETOPT_LONG */
        switch ( arg ) {
            case 'c':
                settings->flags |= SETTINGS_FLAG_CONNECT_ONLY;
                break;
            case 'h':
                settings->flags |= SETTINGS_FLAG_HELP;
                return 0;
            case 'n':
                if ( util_str2ul(optarg, &(settings->numtries), &endptr) != 0 ) {
                    fprintf(stderr, _("Invalid numeric value for tries: %s\n"), optarg);
                    return 1;
                } else if ( settings->numtries == 0 ) {
                    fprintf(stderr, _("Value for tries cannot be zero\n"));
                    return 1;
                } else {
                    break;
                };
            case 'q':
                settings->flags |= SETTINGS_FLAG_QUIET;
                break;
            case 'r':
                settings->flags |= SETTINGS_FLAG_NOCACHE;
                break;
            case 't':
                if ( util_str2ul(optarg, &(settings->timeout), &endptr) != 0 ) {
                    fprintf(stderr, _("Invalid numeric value for timeout: %s\n"), optarg);
                    return 1;
                } else if ( settings->timeout == 0 || settings->timeout > 120 ) {
                    fprintf(stderr, _("Timeout value not in allowed range 1-120: %s\n"), optarg);
                    return 1;
                } else {
                    break;
                };
            case 'V':
                settings->flags |= SETTINGS_FLAG_VERSION;
                return 0;
            case 'v':
                settings->flags |= SETTINGS_FLAG_VERBOSE;
                break;
            case 'w':
                if ( util_str2ul(optarg, &(settings->wait), &endptr) != 0 ) {
                    fprintf(stderr, _("Invalid numeric value for wait: %s\n"), optarg);
                    return 1;
                } else if ( settings->wait == 0 ) {
                    fprintf(stderr, _("Wait time cannot be zero\n"));
                    return 1;
                } else {
                    break;
                };
            case '?':
                return 1;
            default:
                break;
        };
    } while ( arg != -1 );

    if ( optind == argc ) {
        fprintf(stderr, _("Missing host name or address\n"));
        return 1;
    } else if ( argc - optind > 2 ) {
        fprintf(stderr, _("Too many positional arguments\n"));
        return 1;
    };

    settings->hostname = argv[optind++];

    if ( optind != argc )
        settings->service = argv[optind];

    return 0;
}


Settings  *settings_new(void)
{
    Settings        *settings;


    if ( (settings = (Settings*)malloc(sizeof(Settings))) == NULL )
        abort();

    settings->numtries = WAITPORT_DEFAULT_NUMTRIES;
    settings->wait = WAITPORT_DEFAULT_WAIT;
    settings->timeout = WAITPORT_DEFAULT_TIMEOUT;
    settings->flags = SETTINGS_FLAG_NONE;
    settings->hostname = NULL;
    settings->service = WAITPORT_DEFAULT_SERVICE;

    return settings;
}

/* vim: set ft=c sw=4 sts=4 et: */
