/*
 * util.c  - Utility functions functions.
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <errno.h>
#include <limits.h>
#include <stdlib.h>

#include "util.h"


inline
int  util_str2ul(const char  *string, unsigned long int  *result, char  **end)
{
    *result = strtoul(string, end, 10);
    return ((*result == ULONG_MAX || *result == 0) && errno == ERANGE) || *end == string || **end != '\0';
}

/* vim: set ft=c sw=4 sts=4 et: */
