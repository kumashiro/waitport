/*
 * help.c  - Help functions.
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#include <stdio.h>

#include "config.h"
#include "help.h"
#include "gettext.h"


void  help_print(FILE  *stream)
{
    fprintf(stream, _(
        "Usage:\n\n"
        "  %s [ -chqrVv ] [ -n NUMBER ] [ -t SECONDS ] [ -w SECONDS ] HOST [ SERVICE ]\n\n"
        "  -c, --connect-only          do not try to read anything after connect\n"
        "  -h, --help                  print this help screen and exit\n"
        "  -n, --tries NUMBER          try to connect only NUMBER times\n"
        "  -q, --quiet                 do not print status bar\n"
        "  -r, --requery-dns           query name before each probe\n"
        "  -t, --timeout SECONDS       set TCP timeout to SECONDS\n"
        "  -V, --version               print program name, version and exit\n"
        "  -v, --verbose               print probing parameters\n"
        "  -w, --wait SECONDS          set period between probes to SECONDS\n\n"
        "  HOST                        name or IP address of the host\n"
        "  SERVICE                     service name or port number to connect to\n\n"
        "NOTE: long options may not be available on some platforms\n"),
        PACKAGE_NAME
    );
}


void  help_print_version(FILE  *stream)
{
    fprintf(stream, PACKAGE_NAME " " WAITPORT_VERSION "\n"
            "Copyright (C) 2016-2017 Kaito Kumashiro\n"
            "This program comes with ABSOLUTELY NO WARRANTY.\n"
            "This is free software, licensed under the terms of the GNU General Public License.\n"
            "For details, please see the LICENSE.md file that came with this program or point\n"
            "your browser to the web address https://www.gnu.org/licenses/gpl-3.0.en.html on the\n"
            "Internet, SkyNet, Galnet or whatever is available at your time.\n");
}

/* vim: set ft=c sw=4 sts=4 et: */
