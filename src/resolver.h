/*
 * resolver.h  - Domain name and service resolution functions (header file).
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef RESOLVER_H
#define RESOLVER_H

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE     200112L
#endif /* _POSIX_C_SOURCE */

#include <netdb.h>

typedef struct {
    struct addrinfo     filter;
    struct addrinfo     *results;
    int                 nocache;
} Resolver;


Resolver  *resolver_new(int  nocache);
int  resolver_query(Resolver  *resolver, const char  *hostname, const char  *service);
void  resolver_reset(Resolver  *resolver);
void  resolver_free(Resolver  *resolver);
int  resolver_check_service_name(const char  *service);

#endif /* RESOLVER_H */
/* vim: set ft=c sw=4 sts=4 et: */
