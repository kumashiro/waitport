/*
 * settings.h  - Argument parsing functions (header file).
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef _SETTINGS_H
#define _SETTINGS_H

#define IS_SET(F,N)     (((F) & (N)) == (N))


typedef enum {
    SETTINGS_FLAG_NONE = 0,
    SETTINGS_FLAG_HELP = 1,
    SETTINGS_FLAG_QUIET = 2,
    SETTINGS_FLAG_VERSION = 4,
    SETTINGS_FLAG_VERBOSE = 8,
    SETTINGS_FLAG_CONNECT_ONLY = 16,
    SETTINGS_FLAG_NOCACHE = 32
} SettingFlag;


typedef struct {
    const char          *hostname;
    const char          *service;
    unsigned long int   numtries;
    unsigned long int   wait;
    unsigned long int   timeout;
    SettingFlag         flags;
} Settings;


int  settings_parse_args(int  argc, char * const  *argv, Settings  *settings);
Settings  *settings_new(void);

#endif /* _SETTINGS_H */
/* vim: set ft=c sw=4 sts=4 et: */
