/*
 * tcp.c  - TCP connection functions.
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef _POSIX_SOURCE
#define _POSIX_SOURCE
#endif /* _POSIX_SOURCE */

#ifndef _POSIX_C_SOURCE
#define _POSIX_C_SOURCE     200112L
#endif /* _POSIX_C_SOURCE */

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <sys/select.h>
#include <sys/socket.h>
#include <unistd.h>

#include "tcp.h"

typedef enum {
    WAIT_READ,
    WAIT_WRITE
} SocketWaitType;


static
Status  wait_for_socket(int  sock, SocketWaitType  type, long  timeout)
{
    int                 soerr, descs;
    socklen_t           solen;
    fd_set              sock_set;
    struct timeval      tv;


    tv.tv_sec = timeout;
    tv.tv_usec = 0;

    FD_ZERO(&sock_set);
    FD_SET(sock, &sock_set);

    if ( type == WAIT_READ )
        descs = select(sock + 1, &sock_set, NULL, NULL, &tv);
    else
        descs = select(sock + 1, NULL, &sock_set, NULL, &tv);

    if ( descs == 0 ) {
        solen = sizeof(socklen_t);
        getsockopt(sock, SOL_SOCKET, SO_ERROR, &soerr, &solen);
        return soerr == 0 ? TCP_STATUS_TIMEOUT : TCP_STATUS_UNKNOWN;
    } else if ( descs == -1 ) {
        return TCP_STATUS_SYSTEM_ERROR;
    };

    return TCP_STATUS_SUCCESS;
}


static
Status  try_read(int  sock, long  timeout, char  *rbuf, size_t  rbuf_len)
{
    Status              status;


    status = wait_for_socket(sock, WAIT_READ, timeout);
    if ( status == TCP_STATUS_SUCCESS && recv(sock, rbuf, rbuf_len, 0) <= 0 )
        return TCP_STATUS_READ_ERROR;

    return status;
}


void  tcp_addr_to_string(struct addrinfo  *address, char  *buffer, size_t  buflen)
{
    switch ( address->ai_family ) {
        case AF_INET:
            inet_ntop(AF_INET, &(((struct sockaddr_in *)(address->ai_addr))->sin_addr), buffer, buflen);
            break;
        case AF_INET6:
            inet_ntop(AF_INET6, &(((struct sockaddr_in6 *)(address->ai_addr))->sin6_addr), buffer, buflen);
            break;
        default:
            strncpy(buffer, "host", buflen);
            break;
    };
}


Status  tcp_try_connect(struct addrinfo  *addresses, struct addrinfo  *alive, long  timeout, char  *rbuf, size_t  rbuf_len)
{
    int                 sock, soerr;
    struct addrinfo     *addr;
    Status              status = TCP_STATUS_NAME_ERROR;


    for ( addr = addresses; addr != NULL; addr = addr->ai_next ) {
        status = TCP_STATUS_UNKNOWN;
        if ( (sock = socket(addr->ai_family, addr->ai_socktype, addr->ai_protocol)) < 0 ) {
            status = TCP_STATUS_SYSTEM_ERROR;
            continue;
        };

        fcntl(sock, F_SETFL, O_NONBLOCK);
        soerr = connect(sock, addr->ai_addr, addr->ai_addrlen);
        if ( soerr < 0 && errno != EINPROGRESS ) {
            close(sock);
            continue;
        };

        status = wait_for_socket(sock, WAIT_WRITE, timeout);
        if ( status == TCP_STATUS_SUCCESS && rbuf_len > 0 )
            status = try_read(sock, timeout, rbuf, rbuf_len);

        if ( status == TCP_STATUS_SUCCESS )
            memcpy(alive, addr, sizeof(struct addrinfo));

        close(sock);
        if ( status == TCP_STATUS_SUCCESS ) break;
    };

    return status;
}

/* vim: set ft=c sw=4 sts=4 et: */
