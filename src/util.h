/*
 * util.h  - Utility functions (header file).
 *
 * This file is part of waitport project.
 *
 * waitport is  free software: you can  redistribute it and/or modify  it under
 * the  terms of  the  GNU General  Public  License as  published  by the  Free
 * Software Foundation,  either version 3 of  the License, or (at  your option)
 * any later version.
 *
 * waitport is distributed in the hope that  it will be useful, but WITHOUT ANY
 * WARRANTY; without  even the implied  warranty of MERCHANTABILITY  or FITNESS
 * FOR  A PARTICULAR  PURPOSE.  See the  GNU General  Public  License for  more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * waitport. If not, see <http://www.gnu.org/licenses/>.
 *
 * Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
 */

#ifndef UTIL_H
#define UTIL_H

int  util_str2ul(const char  *string, unsigned long int  *result, char  **end);

#endif /* UTIL_H */
/* vim: set ft=c sw=4 sts=4 et: */
