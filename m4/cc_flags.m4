# SYNOPSIS
#
#   AX_CC_FLAGS_OPTIONAL(FLAGS)
#   AX_CC_FLAGS_REQUIRED(FLAGS)
#
# DESCRIPTION
#
#   For each flag in FLAGS, check if $CC compiler supports it and if it does,
#   append flag to $CFLAGS. If compiler does not understand the flag, either do
#   nothing (AX_CC_FLAGS_OPTIONAL) or raise a failure (AX_CC_FLAGS_REQUIRED).
#
# LICENSE
#
#   Copyright (c) 2016-2017 Kaito Kumashiro <kumashiro.kaito@gmail.com>
#
#   This program  is free  software: you  can redistribute  it and/or  modify it
#   under the terms of  the GNU General Public License as  published by the Free
#   Software Foundation,  either version 3 of  the License, or (at  your option)
#   any later version.
#
#   This program is distributed in the hope  that it will be useful, but WITHOUT
#   ANY  WARRANTY;  without even  the  implied  warranty of  MERCHANTABILITY  or
#   FITNESS FOR  A PARTICULAR PURPOSE.  See the  GNU General Public  License for
#   more details.
#
#   You should have received a copy of the GNU General Public License along with
#   This program. If not, see <http://www.gnu.org/licenses/>.
#
#   As  a special  exception, the  respective Autoconf  Macro's copyright  owner
#   gives  unlimited permission  to copy,  distribute and  modify the  configure
#   scripts that are the output of  Autoconf when processing the Macro. You need
#   not  follow the  terms  of the  GNU  General Public  License  when using  or
#   distributing such  scripts, even though  portions of  the text of  the Macro
#   appear in them.  The GNU General Public License (GPL)  does govern all other
#   use of the material that constitutes the Autoconf Macro.
#
#   This special exception to the GPL  applies to versions of the Autoconf Macro
#   released by  the Autoconf Archive. When  you make and distribute  a modified
#   version of the Autoconf Macro, you  may extend this special exception to the
#   GPL to apply to your modified version as well.

#serial 001

AC_DEFUN([AX_CC_FLAGS_OPTIONAL],
  [AC_PREREQ([2.68])dnl
   ac_stored_cflags="$CFLAGS"
   m4_foreach_w([ax_cflag], [$1],
      [AC_MSG_CHECKING([if $CC understands optional ax_cflag flag])
       CFLAGS="ax_cflag"
       AC_COMPILE_IFELSE([AC_LANG_PROGRAM([])],
          [AC_MSG_RESULT([yes])
           ac_stored_cflags="$ac_stored_cflags ax_cflag"],
          [AC_MSG_RESULT([no])])
      ])
   CFLAGS="$ac_stored_cflags"]
)dnl AX_CC_FLAGS_OPTIONAL

AC_DEFUN([AX_CC_FLAGS_REQUIRED],
  [AC_PREREQ([2.68])dnl
   ac_stored_cflags="$CFLAGS"
   m4_foreach_w([ax_cflag], [$1],
      [AC_MSG_CHECKING([if $CC understands required ax_cflag flag])
       CFLAGS="ax_cflag"
       AC_COMPILE_IFELSE([AC_LANG_PROGRAM([])],
          [AC_MSG_RESULT([yes])
           ac_stored_cflags="$ac_stored_cflags ax_cflag"],
          [AC_MSG_FAILURE([$CC does not support ax_cflag flag.])])
      ])
  CFLAGS="$ac_stored_cflags"]
)dnl AX_CC_FLAGS_REQUIRED
