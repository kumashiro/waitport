# SOME DESCRIPTIVE TITLE.
# This file is put in the public domain.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: waitport\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2017-03-09 01:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: ../src/waitport.c:119
#, c-format
msgid ""
"\n"
"Connection to %s succeeded after %lu try\n"
msgid_plural ""
"\n"
"Connection to %s succeeded after %lu tries\n"
msgstr[0] ""
msgstr[1] ""

#: ../src/waitport.c:122
#, c-format
msgid ""
"\n"
"Connection unsuccessful after %lu try\n"
msgid_plural ""
"\n"
"Connection unsuccessful after %lu tries\n"
msgstr[0] ""
msgstr[1] ""

#: ../src/waitport.c:78
#, c-format
msgid " and %lu second wait time.\n"
msgid_plural " and %lu seconds wait time.\n"
msgstr[0] ""
msgstr[1] ""

#: ../src/waitport.c:74
#, c-format
msgid " at most %lu time,"
msgid_plural " at most %lu times,"
msgstr[0] ""
msgstr[1] ""

#: ../src/waitport.c:76
#, c-format
msgid " requerying DNS with each probe,"
msgstr ""

#: ../src/waitport.c:77
#, c-format
msgid " with %lu second timeout"
msgid_plural " with %lu seconds timeout"
msgstr[0] ""
msgstr[1] ""

#: ../src/settings.c:67
#, c-format
msgid "Invalid numeric value for timeout: %s\n"
msgstr ""

#: ../src/settings.c:51
#, c-format
msgid "Invalid numeric value for tries: %s\n"
msgstr ""

#: ../src/settings.c:83
#, c-format
msgid "Invalid numeric value for wait: %s\n"
msgstr ""

#: ../src/settings.c:99
#, c-format
msgid "Missing host name or address\n"
msgstr ""

#: ../src/waitport.c:72
#, c-format
msgid "Probing service %s on host %s"
msgstr ""

#: ../src/settings.c:70
#, c-format
msgid "Timeout value not in allowed range 1-120: %s\n"
msgstr ""

#: ../src/settings.c:102
#, c-format
msgid "Too many positional arguments\n"
msgstr ""

#: ../src/help.c:11
#, c-format
msgid ""
"Usage:\n"
"\n"
"  %s [ -chqrVv ] [ -n NUMBER ] [ -t SECONDS ] [ -w SECONDS ] HOST "
"[ SERVICE ]\n"
"\n"
"  -c, --connect-only          do not try to read anything after connect\n"
"  -h, --help                  print this help screen and exit\n"
"  -n, --tries NUMBER          try to connect only NUMBER times\n"
"  -q, --quiet                 do not print status bar\n"
"  -r, --requery-dns           query name before each probe\n"
"  -t, --timeout SECONDS       set TCP timeout to SECONDS\n"
"  -V, --version               print program name, version and exit\n"
"  -v, --verbose               print probing parameters\n"
"  -w, --wait SECONDS          set period between probes to SECONDS\n"
"\n"
"  HOST                        name or IP address of the host\n"
"  SERVICE                     service name or port number to connect to\n"
"\n"
"NOTE: long options may not be available on some platforms\n"
msgstr ""

#: ../src/settings.c:54
#, c-format
msgid "Value for tries cannot be zero\n"
msgstr ""

#: ../src/settings.c:86
#, c-format
msgid "Wait time cannot be zero\n"
msgstr ""
