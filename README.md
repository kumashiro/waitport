`waitport` is a simple diagnostic tool, that tries to connect to a TCP port
until connection succeedes. Typical applications include:

  * waiting for remote server to go fully up after restart
  * waiting for service to go fully up after reload/restart
  * waiting for DNS servers to propagate changes
  * waiting for router/firewall configuration to activate
  * and more.

It can be used in scripts or in command line.

#### Example usage

Probing port `25` on host `server.lan` may look similar to this:

```sh
$ waitport server.lan 25
TTTTTTTTTTTTTTrrrrrrrrrrrrrrr!
Connection to 10.80.240.14 succeeded after 30 tries
```

Each probe reports status with a single character. It may be one of: **T**
(timeout), **r** (connection suceeded, but failed to read data), **D** (host
name could not be resolved), **E** (system error), **.** (unknown... which
should not happen... theoretically) or **!** (connection succeeded).

Optional port argument can be numeric or symbolic (name from `/etc/services`).
By default, port 22 is used, as SSH is the most common service on Unix servers.

Number of probes, connection timeout and time period between probes can be
changed with `-n`, `-t` and `-w` options. If service uses "client-initiated"
protocol (i.e. PostgreSQL), reading from socket can be inhibited with `-c`
option, however, this will generate false-postives if router/firewall accepts
connections before remote machine opens its ports. Such services are **not**
recommended for probing.

#### Compilation and installation

Below is a list of additional software required or recommended for compilation:

  * `required` GNU autotools (aclocal, autoconf, automake)
  * `required` getopt (preferably with `getopt_long()`)
  * `required` gmake
  * `optional` GNU Gettext with tools (xgettext, msginit, msgmerge, msgfmt)

If *getopt* has no `getopt_long()` function, long options will not be available.
If *Gettext* is not present or disabled, error messages translations will not be
available.

To compile `waitport`, clone it and execute the following commands:

```sh
waitport-clone$ ./autoinit.sh
waitport-clone$ ./configure
waitport-clone$ gmake
...
waitport-clone# gmake install
```

You may wish to alter some options before compiling. This can be done by running
`./configure` with arguments. Try `./configure --help` to get a list of
switches.

When recompiling, it is advisable to start the whole process anew. Resetting the
project to its initial state can be done with command:

```sh
waitport-clone$ gmake squeky-clean
```
